##############################################################################################################################################

This is a personal gallery project created in order to learn installing and using Laragon. Enrolling in KelasProgramming Cikgu Dino, using this project I learned about all the stuff below. This is the 1st week of online learning i.e live class and also recorded video class.

- Siapkan komputer untuk web programming (Pemasangan Laragon)
- Visual Studio Code & Emmet
- Asas Git & Gitlab
- Asas HTML & CSS
- Web Responsif dengan Bootstrap
- Membangunkan Web UI Untuk Aplikasi

##############################################################################################################################################
